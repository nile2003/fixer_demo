from setuptools import setup


setup(name='fixer_demo',
      version='0.1',
      description='Fixer service demo package',
      url='https://gitlab.com/nile2003/fixer_demo',
      author='Hosein',
      author_email='hosein@inprobes.com',
      license='MIT',
      packages=['fixer'],
      zip_safe=False)
